angular.module('myApp.factories', [])



.factory('Students', function() {

    var students = [{
            id: 11,
            firstname: "TOoma",
            middlename: "Tooma",
            lastname: "Bisho",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "6735443",
            mobileNo: "4535430944",
            email: "sto@uni.co"
        },
        {
            id: 22,
            firstname: "Abo treka",
            middlename: "Abo treka",
            lastname: "Abo treka",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "673ffff5443",
            mobileNo: "4535430944",
            email: "abotrika@uni.co"
        },
        {
            id: 33,
            firstname: "Barakat",
            middlename: "Tooma",
            lastname: "Barakat",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "6735443",
            mobileNo: "4535430944",
            email: "sto@uni.co"
        },
        {
            id: 44,
            firstname: "Godi",
            middlename: "Tooma",
            lastname: "Sami",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "6735443",
            mobileNo: "4535430944",
            email: "sto@uni.co"
        }
    ];


    return {
        get: function() {
            return students;
        },
        find: function(index) {
            for (i in students) {
                if (index == students[i].id) {
                    console.log("I found this ", students[i]);
                    return students[i];
                }
            }
        }
    }
})

.factory('Instructor', function() {

    var instructors = [{
            id: 1,
            firstname: "TOoma",
            middlename: "Tooma",
            lastname: "Master",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "6735443",
            mobileNo: "4535430944",
            email: "sto@uni.co",
            bio: "write about yourself..."
        },
        {
            id: 2,
            firstname: "Abo treka",
            middlename: "Abo treka",
            lastname: "Master",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "673ffff5443",
            mobileNo: "4535430944",
            email: "abotrika@uni.co",
            bio: "write about yourself..."
        },
        {
            id: 3,
            firstname: "Barakat",
            middlename: "Tooma",
            lastname: "Master",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "6735443",
            mobileNo: "4535430944",
            email: "sto@uni.co",
            bio: "write about yourself..."
        },
        {
            id: 4,
            firstname: "Godi",
            middlename: "Tooma",
            lastname: "Master",
            address: "Cairo",
            birthdate: "05/05/1991",
            nationalId: "6735443",
            mobileNo: "4535430944",
            email: "sto@uni.co",
            bio: "write about yourself..."
        }
    ];

    return {
        get: function() {
            return instructors;
        },
        find: function(index) {
            for (i in instructors) {
                if (index == instructors[i].id) {
                    return instructors[i];
                }
            }
        }
    }
})

.factory('Courses', function() {

    var courses = [{
            id: 1,
            name: "English",
            instructor: "Tooma",
            weeksNumber: "4",
        },
        {
            id: 2,
            name: "Arabic",
            instructor: "Tooma",
            weeksNumber: "2",
        },
        {
            id: 3,
            name: "Java",
            instructor: "Tooma",
            weeksNumber: "6",
        },
        {
            id: 4,
            name: "Spanish",
            instructor: "Tooma",
            weeksNumber: "2",
        }
    ];

    return {
        get: function() {
            return courses;
        },
        find: function(index) {
            for (i in courses) {
                if (index == courses[i].id) {
                    console.log("I found this ", courses[i]);
                    return courses[i];
                }
            }
        }
    }
});