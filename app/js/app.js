angular.module('myApp', ['myApp.controllers', 'myApp.factories', 'ui.router', 'ngValidate', 'myApp.ElValidation', 'ngSanitize'])


.config(function($stateProvider, $urlRouterProvider) {


    $stateProvider


        .state('student', {
            url: '/student',
            templateUrl: 'app/views/partial-student-list.html',
            controller: 'studentListCTRL'
        })
        .state('studentAdd', {
            url: '/add',
            templateUrl: 'app/views/partial-student-add.html',
            controller: 'studentAddCTRL'
        })
        .state('studentView', {
            url: '/view/:stuId',
            templateUrl: 'app/views/partial-student-view.html',
            controller: 'studentViewCTRL'
        })
        .state('studentEdit', {
            url: '/edit/:stuId',
            templateUrl: 'app/views/partial-student-edit.html',
            controller: 'studentEditCTRL'
        })



    .state('instructor', {
            url: '/instructor',
            templateUrl: 'app/views/partial-instructor-list.html',
            controller: 'instructorListCTRL'
        })
        .state('instructorAdd', {
            url: '/add',
            templateUrl: 'app/views/partial-instructor-add.html',
            controller: 'instructorAddCTRL'
        })
        .state('instructorView', {
            url: '/view/:instId',
            templateUrl: 'app/views/partial-instructor-view.html',
            controller: 'instructorViewCTRL'
        })
        .state('instructorEdit', {
            url: '/edit/:instId',
            templateUrl: 'app/views/partial-instructor-edit.html',
            controller: 'instructorEditCTRL'
        })



    .state('course', {
            url: '/course',
            templateUrl: 'app/views/partial-course-list.html',
            controller: 'courseListCTRL'
        })
        .state('courseAdd', {
            url: '/add',
            templateUrl: 'app/views/partial-course-add.html',
            controller: 'courseAddCTRL'
        })
        .state('courseView', {
            url: '/view/:couId',
            templateUrl: 'app/views/partial-course-view.html',
            controller: 'courseViewCTRL'
        })
        .state('courseEdit', {
            url: '/edit/:couId',
            templateUrl: 'app/views/partial-course-edit.html',
            controller: 'courseEditCTRL'
        })

    ;

    $urlRouterProvider.otherwise('/student');

})

.run(function($state) {
    $state.go('student');
});