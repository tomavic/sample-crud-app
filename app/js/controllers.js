angular.module('myApp.controllers', [])

.controller('studentListCTRL', function($scope, Students) {
    $scope.students = Students.get();
    $scope.deleteStudent = function(id) {
        console.log("Passed student id ", id);
        var _students = $scope.students;
        for (i in _students) {
            if (id == _students[i].id) {
                _students.splice(i, 1);
            }
        }
    }
})

.controller('studentViewCTRL', function($scope, Students, $stateParams) {
    $scope.student = Students.find($stateParams.stuId);
})

.controller('studentAddCTRL', function($scope, $state, Students) {
    var st_arr = Students.get();
    console.log(st_arr);
    $scope.addStudentData = {};

    // Configs for form validation
    $scope.addStudentConfigs = {
        rules: {
            firstname: {
                string: true,
                required: true
            },
            midname: {
                string: true,
                required: true
            },
            lastname: {
                string: true,
                required: true
            },
            address: {
                required: true
            },
            birthdate: {
                required: true
            },
            natId: {
                required: true
            },
            mobno: {
                required: true
            },
            email: {
                required: true,
                validEmail: true
            }
        },
        // validation configs - error messages
        messages: {
            firstname: {
                string: "make it string",
                required: "This field is required"
            },
            midname: {
                string: "make it string",
                required: "This field is required"
            },
            lastname: {
                string: "make it string",
                required: "This field is required"
            },
            address: {
                required: "This field is required"
            },
            birthdate: {
                required: "This field is required"
            },
            natId: {
                required: "This field is required"
            },
            mobno: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                validEmail: "Make sure you entered a Valid Email"
            }
        }
    };

    // function to call after submit the form (if valid)
    function addNewStudent(data) {
        var id = parseInt(st_arr.length) + 1;
        st_arr.push({
            id: id,
            firstname: data.firstname,
            middlename: data.middlename,
            lastname: data.lastname,
            address: data.address,
            birthdate: data.birthdate,
            nationalId: data.nationalId,
            mobileNo: data.mobileNo,
            email: data.email
        });
        $state.go('student');
    }

    // a function to call at the time of submit form
    $scope.addStudent = function(form) {
        var studentData = $scope.addStudentData;
        if (form.validate()) {
            console.log("FORM is valid!")
            console.info('Add Student form data', studentData);
            addNewStudent(studentData);
        }
    };
})

.controller('studentEditCTRL', function($scope, $state, Students, $stateParams) {
    var st_arr = Students.get();
    $scope.studentId = $stateParams.stuId;
    $scope.editStudentData = Students.find($stateParams.stuId);
    console.log("I want to edit student with this id ", $scope.studentId);

    // Configs for form validation
    $scope.editStudentConfigs = {
        rules: {
            firstname: {
                string: true,
                required: true
            },
            midname: {
                string: true,
                required: true
            },
            lastname: {
                string: true,
                required: true
            },
            address: {
                required: true
            },
            birthdate: {
                required: true
            },
            natId: {
                required: true
            },
            mobno: {
                required: true
            },
            email: {
                required: true,
                validEmail: true
            }
        },
        // validation configs - error messages
        messages: {
            firstname: {
                string: "make it string",
                required: "This field is required"
            },
            midname: {
                string: "make it string",
                required: "This field is required"
            },
            lastname: {
                string: "make it string",
                required: "This field is required"
            },
            address: {
                required: "This field is required"
            },
            birthdate: {
                required: "This field is required"
            },
            natId: {
                required: "This field is required"
            },
            mobno: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                validEmail: "Make sure you entered a Valid Email"
            }
        }
    };

    // function to call after submit the form (if valid)
    function saveStudentData(data) {
        var id = $stateParams.stuId;
        for (i in st_arr) {
            if (id == st_arr[i].id) {
                st_arr.splice(i, 1);
                //console.info("We are about to add it again to the array list");
                st_arr.push({
                    id: id,
                    firstname: data.firstname,
                    middlename: data.middlename,
                    lastname: data.lastname,
                    address: data.address,
                    birthdate: data.birthdate,
                    nationalId: data.nationalId,
                    mobileNo: data.mobileNo,
                    email: data.email
                });
            }
            //console.log("Array after update ", st_arr);
        }
        $state.go('student');
    }

    // a function to call at the time of submit form
    $scope.editStudent = function(form) {
        var studentData = $scope.editStudentData;
        if (form.validate()) {
            console.log("FORM is valid!")
            console.info('Edit Student form data', studentData);
            saveStudentData(studentData);
        }
    };
})



.controller('instructorListCTRL', function($scope, Instructor) {
    $scope.instructors = Instructor.get();
    $scope.deleteInstructor = function(id) {
        var _instructors = $scope.instructors;
        for (i in _instructors) {
            if (id == _instructors[i].id) {
                _instructors.splice(i, 1);
            }
        }
    }
})

.controller('instructorViewCTRL', function($scope, Instructor, $stateParams) {
    $scope.instructor = Instructor.find($stateParams.instId);
})

.controller('instructorAddCTRL', function($scope, $state, Instructor) {
    var inst_arr = Instructor.get();
    $scope.addInstructorData = {};

    // Configs for form validation
    $scope.addInstructorConfigs = {
        rules: {
            firstname: {
                string: true,
                required: true
            },
            midname: {
                string: true,
                required: true
            },
            lastname: {
                string: true,
                required: true
            },
            address: {
                required: true
            },
            birthdate: {
                required: true
            },
            natId: {
                required: true
            },
            mobno: {
                required: true
            },
            email: {
                required: true,
                validEmail: true
            }
        },
        // validation configs - error messages
        messages: {
            firstname: {
                string: "make it string",
                required: "This field is required"
            },
            midname: {
                string: "make it string",
                required: "This field is required"
            },
            lastname: {
                string: "make it string",
                required: "This field is required"
            },
            address: {
                required: "This field is required"
            },
            birthdate: {
                required: "This field is required"
            },
            natId: {
                required: "This field is required"
            },
            mobno: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                validEmail: "Make sure you entered a Valid Email"
            }
        }
    };

    // function to call after submit the form (if valid)
    function addNewInstructor(data) {
        var id = parseInt(inst_arr.length) + 1;
        inst_arr.push({
            id: id,
            firstname: data.firstname,
            middlename: data.middlename,
            lastname: data.lastname,
            address: data.address,
            birthdate: data.birthdate,
            nationalId: data.nationalId,
            mobileNo: data.mobileNo,
            email: data.email
        });
        $state.go('instructor');
    }

    // a function to call at the time of submit form
    $scope.addInstructor = function(form) {
        var instructorData = $scope.addInstructorData;
        if (form.validate()) {
            console.log("FORM is valid!")
            console.info('Add instructor form data', instructorData);
            addNewInstructor(instructorData);
        }
    };
})

.controller('instructorEditCTRL', function($scope, $state, Instructor, $stateParams) {
    var inst_arr = Instructor.get();
    $scope.instructorId = $stateParams.instId;
    $scope.editInstructorData = Instructor.find($stateParams.instId);

    // Configs for form validation
    $scope.editInstructorConfigs = {
        rules: {
            firstname: {
                string: true,
                required: true
            },
            midname: {
                string: true,
                required: true
            },
            lastname: {
                string: true,
                required: true
            },
            address: {
                required: true
            },
            birthdate: {
                required: true
            },
            natId: {
                required: true
            },
            mobno: {
                required: true
            },
            email: {
                required: true,
                validEmail: true
            }
        },
        // validation configs - error messages
        messages: {
            firstname: {
                string: "make it string",
                required: "This field is required"
            },
            midname: {
                string: "make it string",
                required: "This field is required"
            },
            lastname: {
                string: "make it string",
                required: "This field is required"
            },
            address: {
                required: "This field is required"
            },
            birthdate: {
                required: "This field is required"
            },
            natId: {
                required: "This field is required"
            },
            mobno: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                validEmail: "Make sure you entered a Valid Email"
            }
        }
    };

    // function to call after submit the form (if valid)
    function saveInstructorData(data) {
        var id = $stateParams.instId;
        for (i in inst_arr) {
            if (id == inst_arr[i].id) {
                inst_arr.splice(i, 1);
                inst_arr.push({
                    id: id,
                    firstname: data.firstname,
                    middlename: data.middlename,
                    lastname: data.lastname,
                    address: data.address,
                    birthdate: data.birthdate,
                    nationalId: data.nationalId,
                    mobileNo: data.mobileNo,
                    email: data.email
                });
            }
        }
        $state.go('instructor');
    }

    // a function to call at the time of submit form
    $scope.editInstructor = function(form) {
        var instructorData = $scope.editInstructorData;
        if (form.validate()) {
            console.log("FORM is valid!")
            console.info('Edit Instructor form data', instructorData);
            saveInstructorData(instructorData);
        }
    };
})




.controller('courseListCTRL', function($scope, Courses) {
    $scope.courses = Courses.get();
    $scope.deleteCourse = function(id) {
        console.log("Passed student id ", id);
        var _courses = $scope.courses;
        for (i in _courses) {
            if (id == _courses[i].id) {
                _courses.splice(i, 1);
            }
        }
    }
})

.controller('courseViewCTRL', function($scope, $stateParams, Courses) {
    $scope.course = Courses.find($stateParams.couId);
})

.controller('courseAddCTRL', function($scope, $state, Courses) {
    var coursesList = Courses.get();
    console.log(coursesList);
    $scope.addCourseData = {};

    // Configs for form validation
    $scope.addCourseConfigs = {
        rules: {
            couName: {
                string: true,
                required: true
            },
            couInst: {
                string: true,
                required: true
            },
            couWeeksno: {
                integer: true,
                required: true
            }
        },
        // validation configs - error messages
        messages: {
            couName: {
                string: "make it string",
                required: "This field is required"
            },
            couInst: {
                integer: "make it string",
                required: "This field is required"
            },
            couWeeksno: {
                integer: "make it number",
                required: "This field is required"
            }
        }
    };

    // function to call after submit the form (if valid)
    function addNewCourse(data) {
        var id = parseInt(coursesList.length) + 1;
        coursesList.push({
            id: id,
            couName: data.couName,
            couInst: data.couInst,
            couWeeksno: data.couWeeksno
        });
        $state.go('course');
    }

    // a function to call at the time of submit form
    $scope.addCourse = function(form) {
        var courseData = $scope.addCourseData;
        if (form.validate()) {
            console.log("FORM is valid!")
            console.info('Add Course form data', courseData);
            addNewCourse(courseData);
        }
    };
})

.controller('courseEditCTRL', function($scope, $state, Courses, $stateParams) {
    var coursesList = Courses.get();
    $scope.courseId = $stateParams.couId;
    $scope.editCourseData = Courses.find($stateParams.couId);
    console.log("I want to edit course with this id ", $scope.courseId);

    // Configs for form validation
    $scope.addCourseConfigs = {
        rules: {
            couName: {
                string: true,
                required: true
            },
            couInst: {
                string: true,
                required: true
            },
            couWeeksno: {
                integer: true,
                required: true
            }
        },
        // validation configs - error messages
        messages: {
            couName: {
                string: "make it string",
                required: "This field is required"
            },
            couInst: {
                string: "make it string",
                required: "This field is required"
            },
            couWeeksno: {
                integer: "make it number",
                required: "This field is required"
            }
        }
    };

    // function to call after submit the form (if valid)
    function saveCourseData(data) {
        var id = $stateParams.couId;
        for (i in coursesList) {
            if (id == coursesList[i].id) {
                coursesList.splice(i, 1);
                coursesList.push({
                    id: id,
                    couName: data.couName,
                    couInst: data.couInst,
                    couWeeksno: data.couWeeksno
                });
            }
        }
        $state.go('course');
    }

    // a function to call at the time of submit form
    $scope.editCourse = function(form) {
        var courseData = $scope.editCourseData;
        if (form.validate()) {
            console.log("FORM is valid!")
            console.info('Edit Course form data', courseData);
            saveCourseData(courseData);
        }
    };
})

;