angular.module('myApp.ElValidation', [])


//configuration for validation module
.config(function($validatorProvider) {

    $validatorProvider.setDefaults({
        debug: true,
        errorElement: 'span',
        errorClass: 'alert'
    });

    $.validator.addMethod('integer', function(value, element) {
        return this.optional(element) || /^\d+$/.test(value);
    });

    $.validator.addMethod('string', function(value, element) {
        return this.optional(element) || /^[a-zA-Z]{4,100}$/g.test(value);
    });

    $.validator.addMethod('validEmail', function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g.test(value);
    });
});